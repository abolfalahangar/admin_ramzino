import Vue from "vue";
import Vuetify from "vuetify/lib/framework";
import ripple from "vuetify/lib/directives/ripple";
import minifyTheme from "minify-css-string";
import "@mdi/font/css/materialdesignicons.css";
Vue.use(Vuetify, { directives: { ripple } });

const themeDark = {
  primary: "#f6f6f6",
  secondary: "#9C27b0",
  accent: "#e91e63",
  info: "#fff",
  success: "#00ff9c",
  success2: "#22ae78",
  green: "#02c076",
  warning: "#FB8C00",
  error: "#ff3b3b",
  background: "#22313a",
  white: "#ffffff",
  //#12161c 181a20 #14151a 22313a #13212a #0d1c24
  main: "#9bbcd1",
  third: "#fff",
  strip: "#17232a",
  blue: "#536dfe",
  blank_layout: "#22313a",
  scroll: "#5e6673",
  track: "#363636",
  aqua: "#00c4ff",
  navbar: "#007f9b",
  sheet: "#0d1c24",
  order_content: "#d8dce1",
  tomato: "#f84960"
};
const themeLight = {
  primary: "#080707",
  secondary: "#9C27b0",
  accent: "#e91e63",
  info: "#000",
  success: "#00ff9c",
  success2: "#22ae78",
  green: "#02c076",
  warning: "#FB8C00",
  error: "#ff3b3b",
  background: "#efefef",
  white: "#ffffff",
  main: "#171d20",
  third: "#73b4dd",
  strip: "#f4f4f4",
  blue: "#536dfe",
  blank_layout: "#22313a",
  scroll: "#b2b4b8",
  track: "#dfdede",
  aqua: "#09b6e9",
  navbar: "#007f9b",
  sheet: "#fafafa",
  order_content: "#474d57",
  tomato: "#f84960"
};

export default new Vuetify({
  breakpoint: { mobileBreakpoint: 960 },
  icons: {
    values: { expand: "mdi-menu-down" }
  },
  // customVariables: ["~/assets/scss/app/variables.scss"],
  treeShake: true,
  rtl: true,
  theme: {
    options: {
      customProperties: true,
      minifyTheme
    },
    dark: true,
    themes: {
      dark: themeDark,
      light: themeLight
    }
  }
});
