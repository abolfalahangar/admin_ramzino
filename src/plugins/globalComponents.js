import Vue from "vue";

import Snackbar from "@/components/Snackbar.vue";
import CKEditor from "ckeditor4-vue";

import VuePersianDatetimePicker from "vue-persian-datetime-picker";

Vue.component("date-picker", VuePersianDatetimePicker);
Vue.component("snackbar", Snackbar);
Vue.use(CKEditor);
