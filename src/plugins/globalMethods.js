import Vue from "vue";
import CryptoJS from "crypto-js";
import store from "../store";

const globalMethods = {
  install(Vue, options) {
    (Vue.prototype.$setDataToLocalStorage = data => {
      let cipherManager = CryptoJS.AES.encrypt(
        JSON.stringify(data),
        process.env.VUE_APP_SECRET_KEY
      ).toString();

      localStorage.setItem("user", cipherManager);
    }),
      (Vue.prototype.$getDataFromLocalStorage = name => {
        let data = localStorage.getItem(name);
        if (data) {
          let bytes = CryptoJS.AES.decrypt(
            data,
            process.env.VUE_APP_SECRET_KEY
          ).toString(CryptoJS.enc.Utf8);
          return JSON.parse(bytes);
        }
        return null;
      }),
      (Vue.prototype.$removeDataFromLocalStorage = name => {
        localStorage.removeItem(name);
      }),
      (Vue.prototype.$showSnackbar = ({
        content = "",
        color = "",
        position = "left"
      }) => {
        store.commit("app/showMessage", { content, color, position });
      }),
      (Vue.prototype.$formatIRNumber = (number, options = {}) => {
        return new Intl.NumberFormat("ir", options).format(number);
      }),
      (Vue.prototype.$isNotFalseValue = value => {
        if (
          value != "" ||
          value != undefined ||
          value != 0 ||
          value != "0" ||
          value != null
        ) {
          return true;
        } else {
          return false;
        }
      }),
      (Vue.prototype.$decrypt = data => {
        let bytes = CryptoJS.AES.decrypt(
          data,
          process.env.VUE_APP_SECRET_KEY
        ).toString(CryptoJS.enc.Utf8);
        return JSON.parse(bytes);
      }),
      (Vue.prototype.$encrypt = data => {
        let bytes = CryptoJS.AES.encrypt(
          JSON.stringify(data),
          process.env.VUE_APP_SECRET_KEY
        ).toString();
        return bytes;
      }),

      Vue.prototype.$isInt = (n)=>{
        return Number(n) === n && n % 1 === 0;
    }
  }
};

Vue.use(globalMethods);
