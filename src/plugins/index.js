import "./vue-meta";
import "./globalMethods";
import "./globalComponents";
import "./directives";
import "nprogress/nprogress.css";
