import Vue from "vue";
import Cleave from "cleave.js";
import { VueMaskDirective } from 'v-mask'

const getInput = el => {
  if (el.tagName.toLocaleUpperCase() !== "INPUT") {
    const els = el.getElementsByTagName("input");
    if (els.length !== 1) {
      throw new Error(`v-cleave requires 1 input, found ${els.length}`);
    } else {
      el = els[0];
    }
  }
  return el;
};

Vue.directive("cleave", {
  inserted: (el, binding) => {
    el = getInput(el);
    el.cleave = new Cleave(el, binding.value || {});
  },
  update: el => {
    el = getInput(el);
    const event = new Event("input", { bubbles: true });
    setTimeout(function(){
      el.value = el.cleave.properties.result;
      el.dispatchEvent(event);
    }, 100);
  }
});



Vue.directive('mask', VueMaskDirective);
