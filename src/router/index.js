// Imports
import Vue from "vue";
import Router from "vue-router";
import NProgress from "../plugins/nprogress";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  scrollBehavior: (to, from, savedPosition) => {
    if (to.hash) return { selector: to.hash };
    if (savedPosition) return savedPosition;
    return { x: 0, y: 0 };
  },

  routes: [
    {
      path: "",
      component: () =>
        import(
          /* webpackChunkName: "default-[request]" */ "@/layouts/default/Index.vue"
        ),
      children: [
        {
          path: "",
          redirect: "/dashboard",
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/dashboard",
          name: "داشبورد",
          component: () =>
            import(
              /* webpackChunkName: "dashboard" */ "@/views/dashboard/Dashboard.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/coins-settings",
          name: "تنظیمات ارز ها",
          component: () =>
            import(
              /* webpackChunkName: "coins-settings" */ "@/views/coins-settings/CoinsSettings.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/coin-setting/:id",
          name: "تنظیمات ارز",
          component: () =>
            import(
              /* webpackChunkName: "coin-setting" */ "@/views/coins-settings/CoinSetting.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/markets-settings",
          name: "تنظیمات بازار ها",
          component: () =>
            import(
              /* webpackChunkName: "markets-settings" */ "@/views/markets-settings/MarketsSettings.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/market-settings/:marketId",
          name: "تنظیمات بازار",
          component: () =>
            import(
              /* webpackChunkName: "market-settings" */ "@/views/markets-settings/MarketSettings.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/users",
          name: "کاربران",
          component: () =>
            import(/* webpackChunkName: "users" */ "@/views/users/Users.vue"),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/User",
          name: "کاربر",
          component: () =>
            import(/* webpackChunkName: "user" */ "@/views/users/User.vue"),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/user-pending",
          name: "کاربر در انتظار",
          component: () =>
            import(
              /* webpackChunkName: "user-pending" */ "@/views/users/UserPending.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/user-profile",
          name: "پروفایل کاربر",
          component: () =>
            import(
              /* webpackChunkName: "user-profile" */ "@/views/users/UserProfile.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/buy-trade-history",
          name: "تاریخچه معاملات خرید",
          component: () =>
            import(
              /* webpackChunkName: "buy-trade-history" */ "@/views/trade-history/BuyTradeHistory.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/sell-trade-history",
          name: "تاریخچه معاملات فروش",
          component: () =>
            import(
              /* webpackChunkName: "sell-trade-history" */ "@/views/trade-history/SellTradeHistory.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/pending-trade-history",
          name: "تاریخچه معاملات باز",
          component: () =>
            import(
              /* webpackChunkName: "pending-trade-history" */ "@/views/trade-history/PendingTradeHistory.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },

        {
          path: "/withdraw-history/:currency",
          name: `تاریخچه برداشت`,
          component: () =>
            import(
              /* webpackChunkName: "withdraw-history" */ "@/views/withdraw-history/WithdrawHistory.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/withdraw-history-user",
          name: "تاریخچه برداشت کاربر فلان",
          component: () =>
            import(
              /* webpackChunkName: "withdraw-history-user" */ "@/views/withdraw-history/WithdrawHistoryUser.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },

        {
          path: "/deposite-history/:currency",
          name: `تاریخچه واریز`,
          component: () =>
            import(
              /* webpackChunkName: "deposite-history" */ "@/views/deposit-history/DepositHistory.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/commission-settings",
          name: "تنظیمات کمیسیون",
          component: () =>
            import(
              /* webpackChunkName: "commission-settings" */ "@/views/commission-settings/CommissionSettings.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/chat",
          redirect: "/maintenance",
          name: "چت",
          component: () =>
            import(
              /* webpackChunkName: "chat" */ "@/views/trade-history/BuyTradeHistory.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },

        {
          path: "/faqs",
          name: "سوالات متداول",
          component: () =>
            import(
              /* webpackChunkName: "faqs" */ "@/views/content-settings/Faqs.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/faq/:id",
          name: "ویرایش سوال",
          component: () =>
            import(
              /* webpackChunkName: "faq" */ "@/views/content-settings/Faq.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/privacy-policy",
          name: "سیاست حفظ حریم خصوصی",
          component: () =>
            import(
              /* webpackChunkName: "privacy-policy" */ "@/views/content-settings/PrivacyPolicy.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/terms",
          name: "قوانین",
          component: () =>
            import(
              /* webpackChunkName: "terms" */ "@/views/content-settings/Terms.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        },
        {
          path: "/social-media",
          name: "شبکه های اجتماعی",
          component: () =>
            import(
              /* webpackChunkName: "social-media" */ "@/views/content-settings/SocialMedia.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: true
          }
        }
      ]
    },
    {
      path: "",
      component: () =>
        import(
          /* webpackChunkName: "default-[request]" */ "@/layouts/blank/Index.vue"
        ),
      children: [
        {
          path: "/login",
          name: "Login",
          component: () =>
            import(
              /* webpackChunkName: "login" */ "@/views/auth-pages/Login.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: false
          }
        },
        {
          path: "/register",
          name: "Register",
          component: () =>
            import(
              /* webpackChunkName: "register" */ "@/views/auth-pages/Register.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: false
          }
        },
        {
          path: "/maintenance",
          name: "Maintenance",
          component: () =>
            import(
              /* webpackChunkName: "maintenance" */ "@/views/utility-pages/Maintenance.vue"
            ),
          meta: {
            rule: "editor",
            requiresAuth: false
          }
        },

        {
          path: "/error-404",
          name: "صفحه مورد نظر پیدا نشد",
          component: () =>
            import(
              /* webpackChunkName: "error-404" */ "@/views/error-pages/Error404.vue"
            ),
          meta: {
            requiresAuth: false
          }
        }
      ]
    },
    {
      path: "*",
      redirect: "/error-404"
    }
  ]
});

// router.beforeEach((to, from, next) => {
//   return to.path.endsWith("/") ? next() : next(trailingSlash(to.path));
// });

router.beforeResolve((to, from, next) => {
  NProgress.start();
  next();
});
router.afterEach((to, from) => {

  NProgress.done();
});

export default router;
