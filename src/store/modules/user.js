// Utilities
import { make } from "vuex-pathify";
let user = localStorage.getItem("user");
user = user != null ? user : null;

const state = {
  drawer: {
    image: 0,
    gradient: 0,
    mini: false
  },
  notifications: [],
  current_user: user
};

const mutations = make.mutations(state);

const actions = {

};

const getters = {
  user_is_authenticated: state => {
    return state.current_user;
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
