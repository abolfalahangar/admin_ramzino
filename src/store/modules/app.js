// Pathify
import { make } from "vuex-pathify";

// Data

const state = {
  items: [
    {
      title: "داشبورد",
      icon: "mdi-view-dashboard",
      to: "/dashboard"
    },
    // {
    //   title: "کاربران",
    //   icon: "mdi-account-multiple",
    //   to: "/users/"
    // },
    {
      title: "تنظیمات ارز ها",
      icon: "mdi-account-multiple",
      to: "/coins-settings"
    },
    {
      title: "تنظیمات بازار ها",
      icon: "mdi-account-multiple",
      to: "/markets-settings"
    },
    // {
    //   title: "تاریخچه معاملات",
    //   icon: "mdi-clipboard-outline",
    //   items: [
    //     {
    //       title: "معاملات خرید",
    //       icon: "mdi-cart",
    //       to: "/buy-trade-history/"
    //     },
    //     {
    //       title: "معاملات فروش",
    //       icon: "mdi-shopping",
    //       to: "/sell-trade-history/"
    //     },
    //     {
    //       title: "سفارشات باز",
    //       icon: "mdi-book-open-blank-variant  ",
    //       to: "/pending-trade-history/"
    //     }
    //   ]
    // },
    // {
    //   title: "تاریخچه واریز کاربر",
    //   icon: "mdi-cash-minus",
    //   items: [
    //     {
    //       title: "BTC",
    //       icon: "mdi-currency-btc",
    //       to: "/deposite-history/BTC"
    //     },
    //     {
    //       title: "ETH",
    //       icon: "mdi-ethereum",
    //       to: "/deposite-history/ETH"
    //     },
    //     {
    //       title: "LTC",
    //       icon: "mdi-litecoin",
    //       to: "/deposite-history/LTC"
    //     },
    //     {
    //       title: "USD",
    //       icon: "mdi-currency-usd",
    //       to: "/deposite-history/USD"
    //     }
    //   ]
    // },
    // {
    //   title: "تاریخچه برداشت کاربر",
    //   icon: "mdi-chart-bubble",
    //   items: [
    //     {
    //       title: "BTC",
    //       icon: "mdi-currency-btc",
    //       to: "/withdraw-history/BTC"
    //     },
    //     {
    //       title: "ETH",
    //       icon: "mdi-ethereum",
    //       to: "/withdraw-history/ETH"
    //     },
    //     {
    //       title: "LTC",
    //       icon: "mdi-litecoin",
    //       to: "/withdraw-history/LTC"
    //     },
    //     {
    //       title: "USD",
    //       icon: "mdi-currency-usd",
    //       to: "/withdraw-history/USD"
    //     }
    //   ]
    // },
    // {
    //   title: "چت",
    //   icon: "mdi-chat-processing",
    //   to: "/chat"
    // },
    // {
    //   title: "تنظیمات کمیسیون",
    //   icon: "mdi-cash",
    //   to: "/commission-settings"
    // },
    // {
    //   title: "تنظیمات محتوی",
    //   icon: "mdi-cog",
    //   items: [
    //     {
    //       title: "قوانین و مقررات",
    //       icon: "mdi-book-cancel-outline",
    //       to: "/terms/"
    //     },
    //     {
    //       title: "سوالات متداول",
    //       icon: "mdi-frequently-asked-questions",
    //       to: "/faqs/"
    //     },
    //     {
    //       title: "حریم خصوصی",
    //       icon: "mdi-message-lock",
    //       to: "/privacy-policy"
    //     },
    //     {
    //       title: "شبکه های اجتماعی",
    //       icon: "mdi-facebook ",
    //       to: "/social-media"
    //     }
    //   ]
    // }
  ],
  drawer: null,
  content: "",
  color: "",
  position: "",
  marketModal: false,
  overlay: false,
  percent: 0,
  mini: false,
  loading : false
};

const mutations = make.mutations(state);

mutations["showMessage"] = (state, payload) => {
  state.content = payload.content;
  state.color = payload.color;
  state.position = payload.position;
};

const actions = {
  ...make.actions(state),
  init: async ({ dispatch }) => {
    //
  },
  logout() {}
};

const getters = {};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
