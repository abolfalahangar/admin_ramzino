const MustAuth = (to, from, next, vm) => {
  if (vm.$store.getters["user/user_is_authenticated"]) {
    next();
  } else {
    if (to.path !== "/login") {
      next({ path: "/login", params: { nextUrl: to.fullPath } });
    } else {
      next();
    }
  }
};

const NotAuth = (to, from, next, vm) => {
  if (vm.$store.getters["user/user_is_authenticated"]) {
    //    { path: from.path }
    next();
  } else {
    if (to.path !== "/login") {
      next({ path: "/login" });
    } else {
      next();
    }
  }
};

export { NotAuth, MustAuth };
