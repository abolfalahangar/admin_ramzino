// axios
import axios from "axios";
import NProgress from "../plugins/nprogress";
import Vue from "vue";
import store from "../store";
import {  get } from "vuex-pathify";

const baseURL = `${process.env.VUE_APP_BASE_URL}/api`;
let http = axios.create({
  baseURL,
  timeout: 8000,
  // withCredentials: true,

  headers: {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Headers": "*",
    "Access-Control-Allow-Methods": "*",
    "Access-Control-Allow-Credentials": true,
    "Cross-Origin-Embedder-Policy": "require-corp",
    "Cross-Origin-Opener-Policy": "same-origin",
    Accept: "application/json",
    "Accept-Language": "en-US,en;q=0.8",
    "Content-Type": "application/json",
    "x-xss-protection": "1; mode=block"
    // Authorization: `Bearer ${token}`
  },
  onDownloadProgress: event => {
    if (event.lengthComputable === true) {
      // console.log("event.lengthComputable", event.lengthComputable);
      let result = Math.round((event.loaded / event.total) * 100);
      let percent = get("app", ["percent"]);
      store.commit("app/percent");
      percent = result;

      // console.log("percent", percent);
      // console.log("store", store.state.app.percent);
    }
  },
  onUploadProgress: event => {
    if (event.lengthComputable === true) {
      // console.log("event.lengthComputable", event.lengthComputable);
      let percent = Math.round((event.loaded / event.total) * 100);
      // console.log("percent", percent);
    }
  }
});
http.interceptors.request.use(
  config => {
    store.commit("app/loading", true);
    NProgress.start();
    return config;
  },
  error => {
    console.log("error request",error);
    console.log("error.message request",error.message);
    store.commit("app/overlay", true);
    store.commit("app/loading", false);
    NProgress.done();
    return Promise.reject(error);
  }
);

http.interceptors.response.use(
  response => {
    NProgress.done();
    store.commit("app/loading", false);
    return response;
  },
  error => {

    console.log("error response",error);
    console.log("error.message response",error.message);
    console.log("error.response response",error.response);
    console.log("error code", error?.code);
    console.log("error stack", error?.stack);
    store.commit("app/loading", false);
    NProgress.done();

    if (typeof error?.response?.data?.message == "string") {
      let err = error?.response?.data?.message;
      Vue.prototype.$showSnackbar({ content: err, color: "red" });
    } 
    // console.log("error response", error);
    if (typeof error?.response?.data?.error === "string") {
      let err = error?.response?.data?.error;
      Vue.prototype.$showSnackbar({ content: err, color: "red" });
    } else if (
      Array.isArray(error?.response?.data?.error) &&
      error?.response?.data?.error?.length > 0 === "string"
    ) {
      error?.response?.data?.error.map((err, index) => {
        Vue.prototype.$showSnackbar({ content: err, color: "red" });
      });
    }

    // if (error.response === undefined) {
    //   errorMessage = "Please check your internet connectivity!";
    // }

    // Handling Expired Token (Forbidden Requests)
    // if (error) {
    //   const originalRequest = error.config;
    //   if (error.response.status === 401 && !originalRequest._retry) {

    //       originalRequest._retry = true;
    //       store.dispatch('LogOut')
    //       return router.push('/login')
    //   }
    // or

    //  if (error.response.status === 403 && !originalRequest._retry) {
    //   originalRequest._retry = true;
    //   const access_token = await refreshAccessToken();
    //   axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
    //   return axiosApiInstance(originalRequest);
    // }
    // }

    // if (error.response.status === 408 || error.code === 'ECONNABORTED'){
    //   return 'timeout';
    // }

    return Promise.reject(error);
  }
);


export default http;
