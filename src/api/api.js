import Vue from "vue";
// const Auth = require("@/api/app/auth").default;
// const SubmitOrder = require("@/api/services/submit-order").default;

import http from "./http";

const factories = {
  //   auth: Auth(http),
  //   SubmitOrder: SubmitOrder(http),
};

Vue.prototype.$http = http;
Vue.prototype.$api = factories;
