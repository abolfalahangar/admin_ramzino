import { mount, createLocalVue } from "@vue/test-utils";
import Vuetify from "vuetify";
import VueRouter from "vue-router";
import Vue from "vue";
import Vuex from "vuex";
import pathify from "@/plugins/vuex-pathify";
import * as modules from "@/store/modules";
import CoinsSettings from "@/views/coins-settings/CoinsSettings";

import "@/plugins/globalMethods";
import "@/plugins/globalComponents";
import "@/api/api";

Vue.use(Vuetify);

jest.mock("axios", () => ({
  create: jest.fn(() => ({
    get: jest.fn(() =>
      Promise.resolve({
        data: {
          depositRecords: [],
          headers: []
        }
      })
    ),
    interceptors: {
      request: {
        use: jest.fn(),
        eject: jest.fn()
      },
      response: {
        use: jest.fn(),
        eject: jest.fn()
      }
    },
    headers: {}
  }))
}));

describe("CoinsSettings", () => {
  let localVue;
  let store;
  let router;
  let vuetify;
  beforeEach(() => {
    localVue = createLocalVue();
    localVue.use(VueRouter);
    localVue.use(Vuex);
    router = new VueRouter();
    vuetify = new Vuetify();
    store = new Vuex.Store({
      modules,
      plugins: [pathify.plugin]
    });
    jest.resetModules();
    jest.clearAllMocks();
  });

  test("CoinsSettings api", () => {
    const wrapper = mount(CoinsSettings, {
      localVue,
      router,
      vuetify,
      store
    });
    const table = wrapper.findComponent({ name: "v-data-table" });
    expect(table.exists()).toBe(true);
  });
});
