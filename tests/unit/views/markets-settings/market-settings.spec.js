import { mount, createLocalVue } from "@vue/test-utils";
import Vuetify from "vuetify";
import VueRouter from "vue-router";
import Vue from "vue";
import Vuex from "vuex";
import pathify from "@/plugins/vuex-pathify";
import * as modules from "@/store/modules";
import MarketSettings from "@/views/markets-settings/MarketSettings";

import "@/plugins/globalMethods";
import "@/plugins/globalComponents";
import "@/api/api";
import "@/plugins/directives";

Vue.use(Vuetify);

jest.mock("axios", () => ({
  create: jest.fn(() => ({
    get: jest.fn(() =>
      Promise.resolve({
        data: {
          depositRecords: [],
          headers: []
        }
      })
    ),
    interceptors: {
      request: {
        use: jest.fn(),
        eject: jest.fn()
      },
      response: {
        use: jest.fn(),
        eject: jest.fn()
      }
    },
    headers: {}
  }))
}));

describe("Market-Settings", () => {
  let localVue;
  let store;
  let router;
  let vuetify;
  beforeEach(() => {
    localVue = createLocalVue();
    localVue.use(VueRouter);
    localVue.use(Vuex);
    router = new VueRouter();
    vuetify = new Vuetify();
    store = new Vuex.Store({
      modules,
      plugins: [pathify.plugin]
    });
    jest.resetModules();
    jest.clearAllMocks();
  });

  test("Market-Settings api", () => {
    const wrapper = mount(MarketSettings, {
      localVue,
      router,
      vuetify,
      store
    });

    const table = wrapper.findComponent({ name: "v-form" });
    expect(table.exists()).toBe(true);
  });
});
