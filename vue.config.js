const webpack = require("webpack");

module.exports = {
  devServer: {
    disableHostCheck: false
  },
  configureWebpack: {
    plugins: [new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)]
  },
  transpileDependencies: ["vuetify"]
};