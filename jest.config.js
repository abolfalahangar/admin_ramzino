module.exports = {
  preset: "@vue/cli-plugin-unit-jest",
  moduleNameMapper: {
    "^.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$":
      "jest-transform-stub"
  },
  transformIgnorePatterns: ["node_modules/(?!(nprogress|epic-spinners)/)"], //| another module
  setupFilesAfterEnv: ["./jest.setup.js"]
};
